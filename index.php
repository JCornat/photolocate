<?php
session_start();

require "vendor/autoload.php";

$app = new \Slim\Slim();
\config\Connexion::connection("config/config.ini");

$env = $app->environment();
if (!defined('ROOT_PATH')) {
    define('ROOT_PATH', $env['SCRIPT_NAME']);
}

/*-----------------------*\
         Index
\*-----------------------*/

$app->get('/', function() use ($app) {
    $c = new \controller\IndexController($app->request);
    $c->index();
})->name('index');

$app->get('/play/', function() use ($app) {
    $c = new \controller\PlayController($app->request);
    $c->index();
})->name('play');

$app->group('/admin/', function() use ($app) {
    $app->get('', function () use ($app) {
        $c = new \controller\AdminController($app->request);
        $c->index();
    })->name('admin');

    $app->get('cities/', function () use ($app) {
        $c = new \controller\CityController($app->request);
        $c->index();
    })->name('adminCity');

    $app->get('cities/add', function () use ($app) {
        $c = new \controller\CityController($app->request);
        $c->add();
    })->name('adminCityAdd');

    $app->post('cities/add', function () use ($app) {
        $c = new \controller\CityController($app->request);
        $c->addPost();
    })->name('adminCityAddPost');

    $app->get('cities/:id', function ($id) use ($app) {
        $c = new \controller\CityController($app->request);
        $c->edit($id);
    })->name('adminCityEdit');

    $app->post('cities/:id', function ($id) use ($app) {
        $c = new \controller\CityController($app->request);
        $c->editPost($id);
    })->name('adminCityEditPost');

    $app->get('difficulties/', function () use ($app) {
        $c = new \controller\DifficultyController($app->request);
        $c->index();
    })->name('adminDifficulty');

    $app->get('difficulties/add', function () use ($app) {
        $c = new \controller\DifficultyController($app->request);
        $c->add();
    })->name('adminDifficultyAdd');

    $app->post('difficulties/add', function () use ($app) {
        $c = new \controller\DifficultyController($app->request);
        $c->addPost();
    })->name('adminDifficultyAddPost');

    $app->get('difficulties/:id', function ($id) use ($app) {
        $c = new \controller\DifficultyController($app->request);
        $c->edit($id);
    })->name('adminDifficultyEdit');

    $app->post('difficulties/:id', function ($id) use ($app) {
        $c = new \controller\DifficultyController($app->request);
        $c->editPost($id);
    })->name('adminDifficultyEditPost');

});

/*-----------------------*\
         Photos
\*-----------------------*/

$app->group('/photos',function() use ($app){
    $app->get('/',function () use ($app){
        $c = new \controller\PhotoController($app->request);
        $c->index();
    })->name('photo');

    $app->get('/edit/:id',function ($id) use ($app){
        $c = new \controller\PhotoController($app->request);
        $c->edit($id);
    })->name('photoEdit');

    $app->post('/edit/:id',function($id) use ($app){
        $c = new \controller\PhotoController($app->request);
        $c->editPost($id);
    })->name('photoEditPost');

    $app->get('/add',function () use ($app){
        $c = new \controller\PhotoController($app->request);
        $c->add();
    })->name('photoAdd');

    $app->post('/add',function () use ($app){
        $c = new \controller\PhotoController($app->request);
        $c->addPost();
    })->name('photoAddPost');
});



/*-----------------------*\
         Users
\*-----------------------*/

$app->group('/login', function() use ($app) {
    $app->get('/', function() use ($app) {
        $c = new \controller\UserController($app->request);
        $c->login();
    })->name('login');

    $app->post('/', function() use ($app) {
        $c = new \controller\UserController($app->request);
        $c->loginPost();
    })->name('loginPost');
});

$app->group('/register', function() use ($app) {
    $app->get('/', function() use ($app) {
        $c = new \controller\UserController($app->request);
        $c->register();

    })->name('register');

    $app->post('/', function() use ($app) {
        $c = new \controller\UserController($app->request);
        $c->registerPost();

    })->name('registerPost');
});

$app->get('/logout/', function() use ($app) {
    $c = new \controller\UserController($app->request);
    $c->deconnexion();
})->name('logout');




/*-----------------------*\
         API
\*-----------------------*/
$app->group('/api', 'APIMiddleWare', function() use ($app) {
    $app->get('/cities', function() use ($app) {
        $c = new \controller\APIController($app->request);
        $c->cities();
    });

    $app->get('/cities/:id', function($id) use ($app) {
        $c = new \controller\APIController($app->request);
        $c->cityDetail($id);
    });

    $app->get('/difficulties', function() use ($app) {
        $c = new \controller\APIController($app->request);
        $c->difficulties();
    });
});

$app->group('/play/games', 'APIMiddleWare', function() use ($app) {
    $app->post('', function() use ($app) {
        $c = new \controller\APIController($app->request);
        $c->createGame();
    });

    $app->get('/:id', function($id) use ($app) {
        $c = new \controller\APIController($app->request);
        $c->getGame($id);
    });

    $app->get('/:id/leaderboard', function($id) use ($app) {
        $c = new \controller\APIController($app->request);
        $c->getLeaderboard($id);
    });

    $app->get('/:id/photos', function($id) use ($app) {
        $c = new \controller\APIController($app->request);
        $c->getPhotos($id);
    })->name('getPhotos');

    $app->put('/:id', function($id) use ($app) {
        $c = new \controller\APIController($app->request);
        $c->updateGame($id);
    } )->name('updateGame');

});

function links() {
    $app = \Slim\Slim::getInstance();
    return [
        'index' => $app->urlFor('index'),
        'play' => $app->urlFor('play'),
        'register' => $app->urlFor('register'),
        'admin' => $app->urlFor('admin'),
        'logout' => $app->urlFor('logout'),
        'login' => $app->urlFor('login'),
        'adminCity' => $app->urlFor('adminCity'),
        'adminCityEdit' => $app->urlFor('adminCityEdit'),
        'adminCityAdd' => $app->urlFor('adminCityAdd'),
        'adminDifficulty' => $app->urlFor('adminDifficulty'),
        'adminDifficultyEdit' => $app->urlFor('adminDifficultyEdit'),
        'adminDifficultyAdd' => $app->urlFor('adminDifficultyAdd'),
        'photo' => $app->urlFor('photo'),
        'photoEdit' => $app->urlFor('photoEdit'),
        'photoAdd' => $app->urlFor('photoAdd')
    ];
}

function generateToken() {
    $tokenBin = openssl_random_pseudo_bytes(20);
    return bin2hex($tokenBin);
}

function APIMiddleWare() {
    $app = \Slim\Slim::getInstance();
    $response = $app->response();
    $response->header('Access-Control-Allow-Origin', '*');
    $response->header('Content-Type', 'application/json');
}

function checkAdmin() {
    $app = \Slim\Slim::getInstance();
    if(!array_key_exists('admin', $_SESSION)) {
        $app->flash('error', 'Vous n\'avez pas les droits nécessaires');
        $app->redirect($app->urlFor('admin'));
    }
}

$app->run();