<?php

namespace view;

class PhotoEditView extends View {

    public function __construct() {
        $this->layout = "photo-edit.html.twig";
    }

}