<?php namespace model;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model{

    public $table = 'Photo';
    public $idTable = 'id';
    public $timestamps = false;


    public function city() {
        return $this->belongsTo('model\City', 'id_city');
    }

    public function games() {
        return $this->belongsToMany('model\Game','Game_Photo','id_photo','id_game');
    }

    public function user(){
        return $this->belongsTo('model\User','id_user');
    }
}