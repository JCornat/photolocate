<?php
namespace controller;

use view\Play;

class PlayController extends BaseController {

    public function index() {
        $view = new Play();
        $view->addVar('link',links());
        $view->addVar('session', $_SESSION);
        $view->addVar('title', 'PhotoLocate - Jouer');
        $env = $this->app->environment();
        $view->addVar('path',$env['SCRIPT_NAME']);
        echo $view->render();
    }

}