<?php
namespace controller;

use view\Home;

class IndexController extends BaseController {

    public function index() {
        $view = new Home();
        $view->addVar('link',links());
        $view->addVar('title', 'PhotoLocate - Accueil');
        $view->addVar('session', $_SESSION);
        $env = $this->app->environment();
        $view->addVar('path',$env['SCRIPT_NAME']);
        echo $view->render();
    }

}