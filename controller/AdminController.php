<?php
namespace controller;

use view\AdminView;

class AdminController extends BaseController {

    public function index() {

        if (!isset($_SESSION['id'])) {
            $this->app->flash('info', 'Veuillez vous connecter');
            $this->app->redirect($this->app->urlFor('login'));
        }

        $view = new AdminView();
        $view->addVar('link',links());
        $view->addVar('session', $_SESSION);
        $view->addVar('title', 'PhotoLocate - Administration');
        $env = $this->app->environment();
        $view->addVar('path',$env['SCRIPT_NAME']);
        echo $view->render();
    }

}