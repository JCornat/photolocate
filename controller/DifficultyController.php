<?php

namespace controller;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use model\City;
use model\Difficulty;
use model\User;
use \exception\AuthException;
use view\DifficultyEditView;
use view\DifficultyView;

class DifficultyController extends BaseController {

    public function index() {
        checkAdmin();
        $difficulties = Difficulty::all();
        foreach ($difficulties as $difficulty) {
            $difficulty['url'] = $this->app->urlFor('adminDifficultyEdit', array('id' => $difficulty->id));
        }

        $view = new DifficultyView();
        $view->addVar('link',links());
        $view->addVar('session', $_SESSION);
        $view->addVar('difficulties', $difficulties);
        $view->addVar('title', 'PhotoLocate - Administration');
        $env = $this->app->environment();
        $view->addVar('path',$env['SCRIPT_NAME']);
        echo $view->render();
    }

    public function edit($id) {
        checkAdmin();
        $difficulty = Difficulty::find($id);

        $view = new DifficultyEditView();
        $view->addVar('link',links());
        $view->addVar('session', $_SESSION);
        $view->addVar('difficulty', $difficulty);
        $view->addVar('formPost', $this->app->urlFor('adminDifficultyEditPost', array('id' => $difficulty->id)));
        $token = generateToken();
        $view->addVar('token', $token);
        $_SESSION['token'] = $token;
        $view->addVar('title', 'PhotoLocate - Administration');
        $env = $this->app->environment();
        $view->addVar('path',$env['SCRIPT_NAME']);
        echo $view->render();
    }

    public function editPost($id) {
        checkAdmin();
        $post = $this->app->request->post();

        if($_SESSION['token'] != $post['token']) {
            $this->app->flash('error', 'Bad token');
            $this->app->redirect($this->app->urlFor('adminDifficultyEdit', array('id' => $id)));
        }

        $difficulty = Difficulty::find($id);
        if($difficulty == null) {
            $this->app->flash('error', 'Unknown difficulty');
            $this->app->redirect($this->app->urlFor('adminDifficulty'));
        }

        if(array_key_exists("delete", $post)) {
            $difficulty->delete();
            $this->app->flash('success', 'Difficulté supprimée avec succès');
            $this->app->redirect($this->app->urlFor('adminDifficulty'));
        } else {
            $difficulty->label = $post['label'];
            $difficulty->distance = $post['distance'];
            $difficulty->nb_photo = $post['nb_photo'];
            $difficulty->save();
            $this->app->flash('success', 'Difficulté modifiée avec succès');
            $this->app->redirect($this->app->urlFor('adminDifficulty'));
        }

    }

    public function add() {
        checkAdmin();
        $view = new DifficultyEditView();
        $view->addVar('link',links());
        $view->addVar('session', $_SESSION);
        $view->addVar('formPost', $this->app->urlFor('adminDifficultyAddPost'));
        $token = generateToken();
        $view->addVar('token', $token);
        $_SESSION['token'] = $token;
        $view->addVar('title', 'PhotoLocate - Administration');
        $env = $this->app->environment();
        $view->addVar('path',$env['SCRIPT_NAME']);
        echo $view->render();
    }

    public function addPost() {
        checkAdmin();
        $post = $this->app->request->post();

        if($_SESSION['token'] != $post['token']) {
            $this->app->flash('error', 'Bad token');
            $this->app->redirect($this->app->urlFor('adminDifficultyAdd'));
        }

        $difficulty = new Difficulty();
        $difficulty->label = $post['label'];
        $difficulty->distance = $post['distance'];
        $difficulty->nb_photo = $post['nb_photo'];
        $difficulty->created_at = date('Y-m-d H:i:s');
        $difficulty->save();

        $this->app->flash('success', 'Difficulté ajoutée avec succès');
        $this->app->redirect($this->app->urlFor('adminDifficulty'));

    }

}