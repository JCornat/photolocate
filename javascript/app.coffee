app = angular.module 'app', ['ngRoute', 'ngSanitize', 'ngAnimate']

app.config ['$routeProvider', ($routeProvider) ->

    #Routing
    $routeProvider
        .when '/', {templateUrl: rootpath+'/template/home.html'}
        .when '/choose-city', {templateUrl: rootpath+'/template/choose-city.html'}
        .when '/choose-difficulty', {templateUrl: rootpath+'/template/choose-difficulty.html'}
        .when '/games', {templateUrl: rootpath+'/template/map.html'}
        .when '/results', {templateUrl: rootpath+'/template/results.html'}
        .otherwise {redirectTo:'/'}

]
